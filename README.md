# BOUNDED BUFFER #

Esempio di utilizzo dei semafori per l'implementazione di un `Buffer` di dimensioni limitate.

* Il programma inizializza due thread `Producer` e `Consumer` che eseguono operazioni concorrenti sull'oggetto `Buffer`.
* Le operazioni eseguite avvengono in mutua esclusione.
* Il thread `Consumer` rimane in attesa se il buffer è vuoto fino a che il thread `Producer` non scrive qualcosa.
* Il thread `Producer` rimane in attesa se il buffer è pieno fino a che il thread `Consumer` non legge qualcosa.
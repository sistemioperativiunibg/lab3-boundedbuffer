package it.unibg.so.lab3;

public class Consumer extends Thread {
	
	private Buffer<Integer> buffer;

	public Consumer(Buffer<Integer> buffer) {
		this.buffer = buffer;
	}
	
	@Override
	public void run() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		buffer.remove();
		buffer.remove();
	}
	

}

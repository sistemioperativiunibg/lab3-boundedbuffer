package it.unibg.so.lab3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class BoundedBuffer<T> implements Buffer<T> {
	
	private List<T> values;
	
	private int bufferSize;
	private int count;
	
	private Semaphore mutex;
	private Semaphore empty;
	private Semaphore full;	

	public BoundedBuffer(int items) {
		bufferSize = items;
		count = 0;
		values = new ArrayList<T>(bufferSize);
		
		mutex = new Semaphore(1);
		empty = new Semaphore(0);
		full = new Semaphore(bufferSize);
	}

	@Override
	public void insert(T item) {
		try {
			full.acquire();
	        mutex.acquire();
	    } catch (InterruptedException e) {
	    	e.printStackTrace();
	    }
	       
	    values.add(count++, item);
	 
	    if (count == bufferSize)
	    	System.out.println("[Producer] insert: " + item + ", Buffer is FULL");
	    else
	    	System.out.println("[Producer] insert: " + item + ", Buffer Size = " +  count);
	 
	    mutex.release();
	    empty.release();		
	}

	@Override
	public T remove() {
		try {
			empty.acquire();
	        mutex.acquire();
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    }
	    
		T item = values.remove(--count);
	 
	    if (count == 0)
	    	System.out.println("[Consumer] remove: " + item + ", Buffer is EMPTY");
	    else
	        System.out.println("[Consumer] remove: " + item + ", Buffer Size = " + count);
	 
	    mutex.release();
	    full.release();
	    return item;
	}
	
	
	public static void main(String[] args){
		Buffer<Integer> buffer = new BoundedBuffer<Integer>(4);

        Thread producerThread = new Producer(buffer);
        Thread consumerThread = new Consumer(buffer);

        producerThread.start();
        try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        consumerThread.start();
	}

}
